# Maplink Maps Api

A api javascript da Maplink é um novo recurso para substituir nossa api legada de exeibição de mapas, que em breve será desativada. Ela utiliza como motor principal a popular ferramenta [Leaflet](https://leafletjs.com/), ao mesmo tempo que oferece uma camada de funções para executar tarefas comuns, como desenhar rotas e pontos de interesse, de forma facilitada, usando os mesmos formatos de dados adotados pelas apis da Maplink Platform.

## Primeiros passos

### Importar a api javascript

A api javascript da Maplink está disponível publicamente na url <https://maps.maplink.global>, e pode ser incluída em qualquer página web facilmente com uma tag `script`:

```html
<script src="https://maps.maplink.global"></script>
```

### Criar objeto do mapa

Com a api já importada, tudo o que é necessário fazer para renderizar um mapa é definir um elemento `div` em sua página, e criar uma instância da classe `MaplinkMap` associada a este elemento.

O construtor da classe espera por dois argumentos obrigatórios: a chave de api da Maplink (a mesma usada para autenticação nas demais apis Maplink Platform), e o `id` do elemento `div` onde o mapa será renderizado.

```html
<div id="map"></div>
```

```javascript
const apiKey = "[api key da minha aplicação]";
const maplink = new MaplinkMap(apiKey, "map");
```

Um terceiro argumento de configurações pode ser passado opcionalmente na construção do mapa. Este argumento deve ser um objeto, onde cada propriedade corresponde a uma definição de configuração. As opções disponíveis são:

- renderType: aceita os valores string "pbf" e "png". Seleciona o formato em que os tiles do mapa serão enviados pelo
servidor. Por padrão, o formato é o protobuf (pbf), que utiliza recurso de aceleração gráfica do navegador por meio da
api WebGL. Em ambientes sem suporte à WebGL, é necessário que seja configurada a opção png, ou o mapa não poderá ser renderizado.

- center: um objeto de coordenada, definindo o ponto central na renderização inicial do mapa.

- zoom: um número que define o nível de zoom inicial. Este valor é repassado diretamente para a instância do leaflet associada ao mapa. Mais informações podem ser encontradas no link: <https://leafletjs.com/examples/zoom-levels>.

## Formatos comuns de parâmetros

### Coordenadas geográficas

Todos os métodos da api que recebem como parâmetro coordenadas geográficas aceitam dois formatos distintos:

- **Maplink Platform**: um objeto contendo as propriedades `latitude` e `longitude`. Este formato é compatível com os retornos das apis da Maplink Platform, como a Trip e a Planning, visando facilitar a integração com elas.

- **Leaflet**: um array contendo os valores de latitude e longitude, respectivamente. É o formato padrão de coordenadas do Leaflet.

### Configurações opcionais

Os métodos que possuem configurações opcionais, como definições de cor e tamanho de formas geométricas, sempre terão o argumento `options`. O valor esperado para este argumento é um objeto cujas propriedades representem as definições de configuração.

## Recursos

### Marcadores

Um marcador é simplesmente um ponto destacado no mapa, que pode ser usado para mostrar pontos de interesse (POIs) ou simplesmente paradas de uma rota, por exemplo.

Para criar um marcador simples, basta usar o método marker, passando um objeto que representa sua coordenada:

```javascript
maplink.marker({
    latitude: -23.986178,
    longitude: -46.308402
});
```
O resultado do código acima será similar ao da imagem:

![alt text](simple-marker.png "Simple marker")

#### Balão de texto (Popup)

Se desejar associar o marcador a um balão de texto, contendo sua descrição, é possível passar como segundo argumento um objeto de configurações contendo a propriedade `popup`. Esta propriedade aceita uma string, que pode conter formatação html:

```javascript
maplink.marker({
    latitude: -23.986437,
    longitude: -46.308303
}, {
    popup: "<strong>Estátua do Pescador</strong>"
});
```

![alt text](marker-with-popup.png "Marker with popup")

#### Ícone customizado

É possível também definir ícones customizados para seus marcadores, ao invés de usar o ícone padrão da Maplink.
Para tal, primeiro deve-se criar um objeto de ícone usando o método `icon`. Ele recebe três parâmetros, na ordem:

- uma string, obrigatória, com o caminho para imagem do ícone, cujo formato deve ser preferencialmente png;
- uma string, opcional, com o caminho para imagem da sombra a ser projetada pelo ícone;
- um objeto, opcional, com propriedades de tamanho (size) e posicionamento (anchor) do ícone, de sua sombra, e dos balões de texto.

Um exemplo de definição completa de ícone com sua sombra ficaria assim:

```javascript
const myIcon = maplink.icon(
    "apontador-icon.png",
    "apontador-icon-shadow.png",
    {
        iconSize: [34, 43],
        shadowSize: [12, 20],
        iconAnchor: [20, 42],
        shadowAnchor: [5, 10],
        popupAnchor: [-3, -32]
    });
```

Sendo a sombra um atributo opcional dos ícones, é possível omití-la. O exemplo acima sem sua sombra ficaria desta forma:

```javascript
const myIcon = maplink.icon(
    "apontador-icon.png",
    null,
    {
        iconSize: [34, 43],
        iconAnchor: [20, 42],
        popupAnchor: [-3, -32]
    });
```

Com o ícone criado, basta passá-lo na propriedade `icon` do objeto de configurações do método `marker`.

```javascript
maplink.marker({
    latitude: -23.986437,
    longitude: -46.308303
}, {
    icon: myIcon
});
```
O método `icon` é um wrapper da função de mesmo nome do Leaflet, assim, para saber mais sobre as configurações de ícones customizados,
acesse sua documentação em <https://leafletjs.com/examples/custom-icons/>. Ícones criados diretamente através do Leaflet podem ser passados 
normalmente para o método `marker`.

### Linhas

Linhas podem ser desenhadas no mapa, geralmente com o propósito de ilustrar rotas entre dois ou mais pontos. O método `line` permite criar facilmente uma linha, passando como argumento um array de objetos de coordenada:

```javascript
const route = [
    {
        "latitude": -23.983273,
        "longitude": -46.299793
    },
    {
        "latitude": -23.982123,
        "longitude": -46.299857
    },
    {
        "latitude": -23.982008,
        "longitude": -46.299494
    }
];

maplink.line(route);
```

O resultado do código acima deve ser similar ao da imagem:

![alt text](simple-line.png "Simple line")

Quando não é passado nenhum tipo de configuração, alguns valores padrão serão utilizados, como por exemplo a cor da linha. O segundo argumento do método aceita um objeto de configuração, que pode conter as propriedades:

- color: uma string de definição da cor da linha, aceita os mesmos valores de propriedades de cor de CSS, como nome de cor ou código hexadecimal. O valor padrão é `"red"`.

- fitBounds: um valor booleano, define se a linha deve ser enquadrada na renderização inicial do mapa. O valor padrão é `true`.

Segue como ficaria o exemplo anterior de linha com a definição de sua cor:

```javascript
maplink.line(route, {
    color: "#ffa500"
});
```

![alt text](simple-line-color.png "Simple line with custom color")

### Acesso ao Leaflet

Para utilizar recursos ou parâmetros do Leaflet que ainda não possuem equivalentes em nossa api javascript, é possível acessar diretamente o Leaflet e seu objeto de mapa pelas propriedades `L` e `map`, respecivamente.

O exemplo abaixo utiliza a função `circle` do Leaflet, para desenhar um círculo:

```javascript
const L = maplink.L;
const map = maplink.map;

L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);
```

Para checar todas as funcionalidades que o Leaflet oferece, consulte a documentação em <https://leafletjs.com/>. A versão utilizada em nossa api atualmente é a 1.7.